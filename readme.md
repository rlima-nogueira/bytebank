## Integração Contínua ## 
A integração contínua é uma prática de desenvolvimento de software de DevOps em que os desenvolvedores, com frequência, juntam suas alterações de código em um repositório central. Depois disso, criações e testes são executados. [Via Aws]('https://aws.amazon.com/pt/devops/continuous-integration/')

</br>

## Entrega Contínua ## 
Entrega contínua é uma abordagem na qual os times de desenvolvimento lançam produtos de qualidade de forma frequente, previsível e automatizada. Em vez de fazer grandes entregas de uma vez, fazem várias pequenas e rápidas — reduzindo as chances de erros e conquistando maior controle de qualidade. [Via Opus Software]('https://www.opus-software.com.br/o-que-e-entrega-continua/')

</br>

## Pipelines ## 
Pipelines de CI/CD consistem em uma série de etapas a serem realizadas para a disponibilização de uma nova versão de um software. Os pipelines de integração e entrega contínuas (CI/CD) são uma prática que tem como objetivo acelerar a disponibilização de softwares, adotando a abordagem de DevOps ou de engenharia de confiabilidade de sites (SRE).

O pipeline de CI/CD inclui monitoramento e automação para melhorar o processo de desenvolvimento de aplicações principalmente nos estágios de integração e teste, mas também na entrega e na implantação. É possível executar manualmente cada etapa do pipeline de CI/CD, mas o real valor dele está na automação. [Via RedHat]('https://www.redhat.com/pt-br/topics/devops/what-cicd-pipeline')